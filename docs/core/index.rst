************************
Core class documentation
************************


.. toctree::
   :maxdepth: 2
   :caption: Components:

   bin_controller
   dt_controller
