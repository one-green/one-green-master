***********
Quick Start
***********



.. toctree::
   :maxdepth: 2
   :caption: Components:

   setup_master
   setup_grafana
   play_with_grafana
